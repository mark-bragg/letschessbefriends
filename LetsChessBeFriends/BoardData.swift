//
//  BoardData.swift
//  LetsChessBeFriends
//
//  Created by Mark Bragg on 9/21/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//


// H|   |   |   |   |   |   |   |   |
// G|   |   |   |   |   |   |   |   |
// F|   |   |   |   |   |   |   |   |
// E|   |   |   |   |   |   |   |   |
// D|   |   |   |   |   |   |   |   |
// C|   |   |   |   |   |   |   |   |
// B|   |   |   |   |   |   |   |   |
// A|   |   |   |   |   |   |   |   |
//    1   2   3   4   5   6   7   8
import Foundation

class BoardData {
    
    var board = Array(repeating: Array(repeating: Position(piece: Piece(), row: 0, col: 0), count: 8), count: 8)
    var lastMove = (row: -1, col: -1)
    
    init() {
        setBoard()
    }
    
    func setBoard() {
        setPawns()
        setMinorPieces()
        setMajorPieces()
        setEmpties()
        setPieceIds()
    }
    
    func setPawns() {
        for i in 0...7 {
            board[6][i] = Position(piece: Pawn(color: .white), row: 6, col: i)
            board[1][i] = Position(piece: Pawn(color: .black), row: 1, col: i)
        }
    }
    
    func setMinorPieces() {
        // KNIGHTS
            //white
        board[7][1] = Position(piece: Piece(kind: .knight, color: .white), row: 7, col: 1)
        board[7][6] = Position(piece: Piece(kind: .knight, color: .white), row: 7, col: 6)
            //black
        board[0][1] = Position(piece: Piece(kind: .knight, color: .black), row: 0, col: 1)
        board[0][6] = Position(piece: Piece(kind: .knight, color: .black), row: 0, col: 6)
        
        // BISHOPS
            //white
        board[7][2] = Position(piece: Piece(kind: .bishop, color: .white), row: 7, col: 2)
        board[7][5] = Position(piece: Piece(kind: .bishop, color: .white), row: 7, col: 5)
            //black
        board[0][2] = Position(piece: Piece(kind: .bishop, color: .black), row: 0, col: 2)
        board[0][5] = Position(piece: Piece(kind: .bishop, color: .black), row: 0, col: 5)
    }
    
    func setMajorPieces() {
        // ROOKS
            //white
        board[7][0] = Position(piece: Piece(kind: .rook, color: .white), row: 7, col: 0)
        board[7][7] = Position(piece: Piece(kind: .rook, color: .white), row: 7, col: 7)
            //black
        board[0][0] = Position(piece: Piece(kind: .rook, color: .black), row: 0, col: 0)
        board[0][7] = Position(piece: Piece(kind: .rook, color: .black), row: 0, col: 7)
        
        // QUEENS
            //white
        board[7][3] = Position(piece: Piece(kind: .queen, color: .white), row: 7, col: 3)
            //black
        board[0][3] = Position(piece: Piece(kind: .queen, color: .black), row: 0, col: 3)
        
        // KINGS
            //white
        board[7][4] = Position(piece: Piece(kind: .king, color: .white), row: 7, col: 4)
            //black
        board[0][4] = Position(piece: Piece(kind: .king, color: .black), row: 0, col: 4)
    }
    
    func setEmpties() {
        for i in 2...5 {
            for j in 0...7 {
                board[i][j] = Position(piece: Piece(), row: i, col: j)
            }
        }
    }
    
    func setPieceIds() {
        for col in board {
            for i in 0...7 {
                col[i].piece.id = i
            }
        }
    }

    func getPiece(at position: Position) -> Piece {
        return board[position.row][position.col].piece
    }
    
    func getPiece(at position: (Int, Int)) -> Piece {
        return board[position.0][position.1].piece
    }
    
    func getKind(position: (Int, Int)) -> Piece.Kind {
        return board[position.0][position.1].piece.kind
    }
    
    func getPosition(at pos: (Int, Int)) -> Position {
        return board[pos.0][pos.1]
    }
    
    func swapPieces(from position_i: Position, to position_f: Position) {
        let temp = position_i
        position_i.piece = position_f.piece
        position_f.piece = temp.piece
    }
    
    func movePiece(from position_i: Position, to position_f: Position) {
        let tempPiece = position_i.piece
        position_i.piece = Piece()
        position_f.piece = tempPiece
    }
    
}
