//
//  MoveChecker.swift
//  LetsChessBeFriends
//
//  Created by Mark Bragg on 9/20/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//

import Foundation

class MoveChecker {
    
    var boardController: BoardViewController?
    var boardData: BoardData
    var position_f: Position?
    var position_i: Position?
    var piece: Piece?
    
    var slopeOfMovement: (colChange: Double, rowChange: Double) {
        get {
            let num = Double((position_f?.col)! - (position_i?.col)!)
            let den = Double((position_f?.row)! - (position_i?.row)!)
            return (num, den)
        }
    }
    
    init(controller: BoardViewController) {
        boardController = controller
        boardData = controller.boardData
    }
    
    init(boardData: BoardData) {
        self.boardData = boardData
    }
    
    
    func move(from position: Position, to position_f: Position) -> Bool {
        var res = false
        self.position_i = position
        self.position_f = position_f
        let kind = (position_i?.piece.kind)!
        self.piece = position_i?.piece
        
        switch kind {
        case .empty:
            break
        case .pawn:
            //pawn stuff
            print(kind)
            self.piece = piece as! Pawn
            if (piece?.isFirstMove)! {
                (piece as! Pawn).isSecondMove = true
            } else if (piece as! Pawn).isSecondMove {
                (piece as! Pawn).isSecondMove = false
            }
            res = isPawnMove()
        case .bishop:
            //bishop stuff
            print(kind)
        case .knight:
            //knight stuff
            print(kind)
        case .rook:
            //rook stuff
            print(kind)
        case .queen:
            //queen stuff
            print(kind)
        case .king:
            //king stuff
            print(kind)
        }
        
        if res && !isDiscoveredCheck() {
            if (piece?.isFirstMove)! {
                piece?.isFirstMove = false
            }
            boardData.lastMove = (position_f.row, position_f.col)
            // NOTE: - FOR TESTING PURPOSES ONLY
            boardData.movePiece(from: position_i!, to: position_f)
            return true
        }
        return false
    }
    
    // MARK: - Pawn
    func isPawnMove() -> Bool {
        var res = isForwardMove()
        
        if isPawnAttacking() {
            res = res && isLegalPawnAttack()
        } else {
            res = res && isLegalPawnMove()
        }
        
        return res
    }
    
    func isForwardMove() -> Bool {
        let som = slopeOfMovement
        let rowChange = slopeOfMovement.rowChange
        switch (piece?.color)! {
        case .white:
            return rowChange < 0
        case .black:
            return rowChange > 0
        }
    }
    
    func isPawnAttacking() -> Bool {
        return slopeOfMovement.colChange != 0
    }
    
    func isLegalPawnAttack() -> Bool {
        let finalPositionPiece = boardData.getPiece(at: ((position_f?.row)!, (position_f?.col)!))
        
        guard finalPositionPiece.color == self.piece?.color && finalPositionPiece.kind != .empty else {
            let som = slopeOfMovement
            if position_f?.piece.kind == .empty {
                return isEnPassant()
            }
            switch (piece?.color)! {
            case .white:
                return slopeOfMovement.rowChange == -1 && abs(slopeOfMovement.colChange) == 1
            case .black:
                return slopeOfMovement.rowChange == 1 && abs(slopeOfMovement.colChange) == 1
            }
        }
        
        return false
    }
    
    func isLegalPawnMove() -> Bool {
        let res = (position_f?.piece.kind)! == .empty && slopeOfMovement.colChange == 0
        switch (piece?.color)! {
        case .white:
            return (slopeOfMovement.rowChange == -1 || (slopeOfMovement.rowChange == -2 && (piece?.isFirstMove)!)) && res
        case .black:
            return (slopeOfMovement.rowChange == 1 || (slopeOfMovement.rowChange == 2 && (piece?.isFirstMove)!))  && res
        }
    }
    
    func isEnPassant() -> Bool {
        if let attackedPiece = boardData.getPiece(at: ((position_i?.row)!, (position_f?.col)!)) as? Pawn,
            let lastMovePiece = (boardData.getPiece(at: boardData.lastMove) as? Pawn) {
            
            return (attackedPiece.isSecondMove && lastMovePiece.id == attackedPiece.id) && abs(slopeOfMovement.colChange) == 1 && piece?.color == .white ? slopeOfMovement.rowChange == -1 : slopeOfMovement.rowChange == 1
        }
        return false
    }
    
    // MARK: - Miscellaneous
    func isDiscoveredCheck() -> Bool {
        return false
    }
    
}
