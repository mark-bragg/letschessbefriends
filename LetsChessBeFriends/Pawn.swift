//
//  Pawn.swift
//  LetsChessBeFriends
//
//  Created by Mark Bragg on 9/21/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//

import Foundation

class Pawn: Piece {
    var isSecondMove: Bool
    
    init(color: Color) {
        
        isSecondMove = false
        super.init(kind: .pawn, color: color)
    }
}
