//
//  Piece.swift
//  LetsChessBeFriends
//
//  Created by Mark Bragg on 9/20/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//

import Foundation

class Piece {
    
    let kind: Kind
    let color: Color
    var isFirstMove: Bool
    var id = 0
    
    
    init(kind: Kind, color: Color) {
        isFirstMove = true
        self.kind = kind
        self.color = color
    }
    
    init() {
        isFirstMove = true
        self.kind = .empty
        self.color = .black
    }
    
}


// MARK: - Piece Properties
extension Piece {
    enum Kind {
        case pawn
        case bishop
        case knight
        case rook
        case queen
        case king
        
        case empty
    }
    
    enum Color {
        case black
        case white
    }
}
