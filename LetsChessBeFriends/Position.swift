//
//  Position.swift
//  LetsChessBeFriends
//
//  Created by Mark Bragg on 9/21/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//

import Foundation

class Position {
    var piece: Piece
    var row: Int
    var col: Int
    
    init(piece: Piece, row: Int, col: Int) {
        self.piece = piece
        self.row = row
        self.col = col
    }
}
