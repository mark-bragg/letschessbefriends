//
//  LetsChessBeFriendsTests.swift
//  LetsChessBeFriendsTests
//
//  Created by Mark Bragg on 9/20/17.
//  Copyright © 2017 Mark Bragg. All rights reserved.
//

import XCTest
@testable import LetsChessBeFriends

class LetsChessBeFriendsTests: XCTestCase {
    var checker = MoveChecker(boardData: BoardData());
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBoardSetup() {
        var str = ""
        for i in 0..<8 {
            for j in 0..<8 {
                str += "\(checker.boardData.board[i][j].piece.kind)\t"
            }
            str += "\n"
        }
        print(str)
    }
    
    // MARK: - Pawn Tests
    func testBlackPawnMoves() {
        var pos_i: Position, pos_f: Position
        
        // One step first moves
        for i in 0...7 {
            pos_i = checker.boardData.getPosition(at: (1,i))
            pos_f = checker.boardData.getPosition(at: (2,i))
            XCTAssert(checker.move(from: pos_i, to: pos_f))
        }
        checker.boardData.setBoard()
        // Two step first moves
        for i in 0...7 {
            pos_i = checker.boardData.getPosition(at: (1,i))
            pos_f = checker.boardData.getPosition(at: (3,i))
            XCTAssert(checker.move(from: pos_i, to: pos_f))
        }
        
    }
    
    func testWhitePawnMoves() {
        var pos_i: Position, pos_f: Position
        
        // One step first moves
        for i in 0...7 {
            pos_i = checker.boardData.getPosition(at: (6,i))
            pos_f = checker.boardData.getPosition(at: (5,i))
            XCTAssert(checker.move(from: pos_i, to: pos_f))
        }
        checker.boardData.setBoard()
        // Two step first moves
        for i in 0...7 {
            pos_i = checker.boardData.getPosition(at: (6,i))
            pos_f = checker.boardData.getPosition(at: (4,i))
            XCTAssert(checker.move(from: pos_i, to: pos_f))
        }
        
    }
    
    func testWhitePawnAttacks() {
        var blackPosI: Position
        var blackPosF: Position
        var whitePosI: Position
        var whitePosF: Position
        
        // STARTING POSITION FROM RIGHT TO LEFT
        for i in 0...6 {
            blackPosI = checker.boardData.getPosition(at: (1,i))
            blackPosF = checker.boardData.getPosition(at: (5,i))
            checker.boardData.movePiece(from: blackPosI, to: blackPosF)
            whitePosI = checker.boardData.getPosition(at: (6,i+1))
            whitePosF = checker.boardData.getPosition(at: (5,i))
            XCTAssert(checker.move(from: whitePosI, to: whitePosF))
        }
        
        resetBoard()
        // STARTING POSITION FROM LEFT TO RIGHT
        for i in 1...7 {
            blackPosI = checker.boardData.getPosition(at: (1,i))
            blackPosF = checker.boardData.getPosition(at: (5,i))
            checker.boardData.movePiece(from: blackPosI, to: blackPosF)
            whitePosI = checker.boardData.getPosition(at: (6,i-1))
            whitePosF = checker.boardData.getPosition(at: (5,i))
            XCTAssert(checker.move(from: whitePosI, to: whitePosF))
        }
        
        resetBoard()
        // ATTACKING ALL MINOR AND MAJOR PIECES
        for i in 0...5 {
            blackPosI = checker.boardData.getPosition(at: (0,i))
            blackPosF = checker.boardData.getPosition(at: (5,i))
            checker.boardData.movePiece(from: blackPosI, to: blackPosF)
            whitePosI = checker.boardData.getPosition(at: (6,i+1))
            whitePosF = checker.boardData.getPosition(at: (5,i))
            XCTAssert(checker.move(from: whitePosI, to: whitePosF))
        }
        
        resetBoard()
        // ENPASSANT
        for i in 0...5 {
            checker.boardData.movePiece(from: checker.boardData.getPosition(at: (6,i)), to: checker.boardData.getPosition(at: (3,i)))
            testBoardSetup()
            blackPosI = checker.boardData.getPosition(at: (1,i+1))
            blackPosF = checker.boardData.getPosition(at: (3,i+1))
            checker.move(from: blackPosI, to: blackPosF)
            testBoardSetup()
            whitePosI = checker.boardData.getPosition(at: (3,i))
            whitePosF = checker.boardData.getPosition(at: (2,i+1))
            XCTAssert(checker.move(from: whitePosI, to: whitePosF))
            testBoardSetup()
        }
        
    }
    
    func testBlackPawnAttacks() {
        var blackPosI: Position
        var blackPosF: Position
        var whitePosI: Position
        var whitePosF: Position
        
        // STARTING POSITION FROM RIGHT TO LEFT
        for i in 0...6 {
            whitePosI = checker.boardData.getPosition(at: (6,i))
            whitePosF = checker.boardData.getPosition(at: (2,i))
            checker.boardData.movePiece(from: whitePosI, to: whitePosF)
            
            blackPosI = checker.boardData.getPosition(at: (1,i+1))
            blackPosF = checker.boardData.getPosition(at: (2,i))
            XCTAssert(checker.move(from: blackPosI, to: blackPosF))
        }
        
        resetBoard()
        // STARTING POSITION FROM LEFT TO RIGHT
        for i in 1...7 {
            whitePosI = checker.boardData.getPosition(at: (6,i))
            whitePosF = checker.boardData.getPosition(at: (2,i))
            checker.boardData.movePiece(from: whitePosI, to: whitePosF)
            
            blackPosI = checker.boardData.getPosition(at: (1,i-1))
            blackPosF = checker.boardData.getPosition(at: (2,i))
            XCTAssert(checker.move(from: blackPosI, to: blackPosF))
        }
        
        resetBoard()
        // ATTACKING ALL MINOR AND MAJOR PIECES
        for i in 0...5 {
            whitePosI = checker.boardData.getPosition(at: (7,i))
            whitePosF = checker.boardData.getPosition(at: (2,i))
            checker.boardData.movePiece(from: whitePosI, to: whitePosF)
            
            blackPosI = checker.boardData.getPosition(at: (1,i+1))
            blackPosF = checker.boardData.getPosition(at: (2,i))
            XCTAssert(checker.move(from: blackPosI, to: blackPosF))
        }
        
        resetBoard()
        // ENPASSANT
        for i in 0...5 {
            checker.boardData.movePiece(from: checker.boardData.getPosition(at: (1,i)), to: checker.boardData.getPosition(at: (4,i)))
            
            testBoardSetup()
            whitePosI = checker.boardData.getPosition(at: (6,i+1))
            whitePosF = checker.boardData.getPosition(at: (4,i+1))
            checker.move(from: whitePosI, to: whitePosF)
            
            testBoardSetup()
            blackPosI = checker.boardData.getPosition(at: (4,i))
            blackPosF = checker.boardData.getPosition(at: (5,i+1))
            XCTAssert(checker.move(from: blackPosI, to: blackPosF))
            testBoardSetup()
        }
        
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func resetBoard() {
        checker.boardData.setBoard()
    }
    
}
